var app = angular.module('aitu-project', []);

app.controller('ProductCtrl', function($scope, $http) {
    $scope.productList = [];
    $scope.categoryList = [];

    $scope.getProducts = function() {
        $http({
            url: 'http://127.0.0.1:8088/api/products',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.productList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getCategories = function() {
        $http({
            url: 'http://127.0.0.1:8088/api/categories',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.categoryList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getProductsByCategory = function(categoryID) {
        $http({
            url: 'http://127.0.0.1:8088/api/products/category/' + categoryID,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.productList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getCategories();
    $scope.getProducts();



    $scope.auth = {
        login: '',
        password: ''
    };

    $scope.customer = {};

    $scope.login = function(auth) {
        $http({
            url: 'http://127.0.0.1:8088/login',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            data: auth
        })
            .then(function (response) {
                    $scope.auth = response.data;

                    $scope.getMe();
                    console.log(response);
                },
                function (response) { // optional
                    $scope.auth = {};
                });
    };

    $scope.getMe = function() {
        $http({
            url: 'http://127.0.0.1:8088/customers/me',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth": $scope.auth.token
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.customer = response.data;
                },
                function (response) { // optional
                    console.log(response);
                    $scope.customer = {};
                });
    };

    let orderItemList = {};

    $scope.incrementProduct = function (product) {
        if(orderItemList[product.id] === undefined) orderItemList[product.id] = {productID: product.id, price: product.price, quantity: 0};

        orderItemList[product.id].quantity = orderItemList[product.id].quantity + 1;
    };


    let customer_order_id = 0;
    $scope.totalPrice = '';


    $scope.order_data = {
        total_price: $scope.totalPrice,
        customer_id: $scope.getMe()

    };

    $scope.insertCustomer_order = function () {
        $scope.CountTotalPrice();
        $http({
            url: 'http://127.0.0.1:8088/api/customer_orders',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Token": $scope.auth.token

            },
            data: {
                'total_price': $scope.totalPrice
            }
        })
            .then(function (response) {

                    console.log(response);
                },
                function (response) { // optional
                    $scope.order_data= {};
                });
    };




    $scope.order_item_details = {
        quantity: '',
        product_id:'',
        order_id: ''
    };



    $scope.detailedOrderProductList = {};
    $scope.addProduct = function (product){
        if ($scope.detailedOrderProductList[product.id] === undefined) {
            $scope.detailedOrderProductList[product.id] =  {id: product.id, name:product.name, description: product.description, price: product.price, quantity: 1};
        }
        $scope.order_item_details.product_id = product.id;

    };

    $scope.CountTotalPrice = function (){
        $scope.totalPrice = 0;
        angular.forEach($scope.detailedOrderProductList, function (value){
            $scope.totalPrice += value.price * value.quantity;
        });

        console.log($scope.detailedOrderProductList);
    };

    $scope.removeProduct = function (product){
        delete $scope.detailedOrderProductList[product.id];
    };

    $scope.addQuantity = function (product){
        $scope.detailedOrderProductList[product.id].quantity += 1;
        $scope.order_item_details.quantity = $scope.detailedOrderProductList[product.id].quantity;
    };

    let reorganize = function (){
        $scope.products_ids = [];
        $scope.products_quantities = [];
        angular.forEach($scope.detailedOrderProductList, function (value){
            $scope.products_ids.push(value.id);
            if ($scope.products_ids[value.id] === undefined) {
                $scope.products_quantities[value.id] =  value.quantity;
            }
        });
    }

    $scope.insertOrder_item = function () {
        reorganize();
        $http({
            url: 'http://127.0.0.1:8088/api/order_items',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            data: {
                'products_ids' : $scope.products_ids,
                'products_quantities' : $scope.products_quantities
            }
        })
            .then(function (response) {
                    console.log(response);
                },
                function (response) { // optional
                });
    };

    $scope.customer_orderList = [];
    $scope.getCustomer_order = function() {
        $http({
            url: 'http://127.0.0.1:8088/api/customer_orders/customers/'+ $scope.customer.id,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.customer_orderList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getCustomer_orderByStatus = function(status) {
        $http({
            url: 'http://127.0.0.1:8088/api/customer_orders/customers/'+ $scope.customer.id + '/status/' + status,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.customer_orderList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.deleteCustomer_order = function (customer_order) {
        $http({
            url: 'http://127.0.0.1:8088/api/customer_orders/'+ customer_order.id,
            method: "DELETE",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);

                },
                function (response) { // optional
                    console.log(response);
                });

    }




});
