package kz.aitu.testjava.controller;

import kz.aitu.testjava.entity.Customer_order;
import kz.aitu.testjava.service.Customer_orderService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class Customer_orderController {

    private Customer_orderService customer_orderService;


    @GetMapping("/api/customer_orders")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(customer_orderService.getAll());
    }

    @GetMapping("/api/customer_orders/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(customer_orderService.getById(id));
    }

    @GetMapping("/api/customer_orders/customers/{id}")
    public ResponseEntity<?> getByCustomers_id(@PathVariable("id") Long id) {
        return ResponseEntity.ok(customer_orderService.getByCustomer_id(id));
    }
    @GetMapping("/api/customer_orders/customers/{id}/status/{state}")
    public ResponseEntity<?> getByCustomers_id(@PathVariable("id") long id, @PathVariable("state") String state) {
        return ResponseEntity.ok(customer_orderService.getByStatusAndCustomerId(id,state));
    }

    @GetMapping("/api/customer_orders/me")
    public ResponseEntity<?> lastCustomer_id() {
        return ResponseEntity.ok(customer_orderService.lastCustomer_order());
    }

    @PostMapping("/api/customer_orders")
    public void createCustomer_order(@RequestBody Customer_order customer_order,@RequestHeader ("token") String token) throws Exception {

         customer_orderService.insert(customer_order.getTotal_price(),token);

    }

    @PutMapping("/api/customer_orders")
    public ResponseEntity<?> updateCustomer_order(@RequestBody Customer_order customer_order) {
        return ResponseEntity.ok(customer_orderService.update(customer_order));
    }

    @DeleteMapping("/api/customer_orders/{id}")
    public void deleteCustomer_order(@PathVariable("id") long id) {
        customer_orderService.deleteById(id);
    }

}
