package kz.aitu.testjava.controller;

import kz.aitu.testjava.entity.Customer_order;
import kz.aitu.testjava.entity.Order_item;
import kz.aitu.testjava.entity.Order_item_list;
import kz.aitu.testjava.service.Order_itemService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class Order_itemController {

    private Order_itemService order_itemService;


    @GetMapping("/api/order_items")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(order_itemService.getAll());
    }

    @GetMapping("/api/order_items/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(order_itemService.getById(id));
    }

    @PostMapping("/api/order_items")
    public void createOrder_item(@RequestBody Order_item_list order_item_list) {
        order_itemService.insert(order_item_list);
    }

    @PutMapping("/api/order_items")
    public ResponseEntity<?> updateOrder_item(@RequestBody Order_item order_item) {
        return ResponseEntity.ok(order_itemService.update(order_item));
    }

}
