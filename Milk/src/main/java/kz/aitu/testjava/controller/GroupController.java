package kz.aitu.testjava.controller;

import kz.aitu.testjava.entity.Group;
import kz.aitu.testjava.service.GroupService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class GroupController {

    private GroupService groupService;


    @GetMapping("/api/groups")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(groupService.getAll());
    }

    @GetMapping("/api/groups/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(groupService.getById(id));
    }

    @PostMapping("/api/groups")
    public ResponseEntity<?> createGroup(@RequestBody Group group) {
        return ResponseEntity.ok(groupService.save(group));
    }

    @PutMapping("/api/groups")
    public ResponseEntity<?> updateGroup(@RequestBody Group group) {
        return ResponseEntity.ok(groupService.update(group));
    }
}
