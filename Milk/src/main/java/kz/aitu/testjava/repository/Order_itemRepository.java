package kz.aitu.testjava.repository;

import kz.aitu.testjava.entity.Order_item;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface Order_itemRepository extends CrudRepository<Order_item, Long> {
    @Transactional
    @Modifying
    @Query(
            value = "insert into Order_item (product_id,quantity,order_id) values (:product_id,:quantity,:order_id)",
            nativeQuery = true)
    void insertOrder_item(@Param("product_id") int product_id, @Param("quantity") int quantity, @Param("order_id") int order_id);

    List<Order_item> findAll();

    void deleteAllByOrderId(long orderId);


}
