package kz.aitu.testjava.repository;

import kz.aitu.testjava.entity.Customer_order;
import lombok.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface Customer_orderRepository extends CrudRepository<Customer_order, Long> {
    @Transactional
    @Modifying
    @Query(
            value = "insert into Customer_order (total_price,customer_id,status,order_date) values (:total_price,:customer_id,:status,:order_date)",
            nativeQuery = true)
    void insertCustomer_order(@Param("total_price") long total_price, @Param("customer_id") long customer_id, @Param("status") String status,@Param("order_date") LocalDate order_date);


    List<Customer_order> findAll();

    @Query(
            value = "SELECT id FROM customer_order  \n" +
                    "ORDER BY id DESC  \n" +
                    "LIMIT 1;  ",
            nativeQuery = true)
    int lastCustomer_order();

    List<Customer_order> findAllByCustomerId(long Customer_id);

    List<Customer_order> findAllByStatusAndCustomerId(String Status, long Customer_id);

}
