package kz.aitu.testjava.repository;

import kz.aitu.testjava.entity.Customer;
import kz.aitu.testjava.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {


}
