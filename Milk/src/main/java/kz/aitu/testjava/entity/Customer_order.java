package kz.aitu.testjava.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "customer_order")
@NoArgsConstructor
@AllArgsConstructor
public class Customer_order {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long total_price;
    private long customerId;
    private String status;
    private String order_date;
}
