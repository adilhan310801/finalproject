package kz.aitu.testjava.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;

@Data
@Entity
@Table(name = "order_item")
@NoArgsConstructor
@AllArgsConstructor
public class Order_item {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long product_id;
    private long quantity;
    private long orderId;
}
