package kz.aitu.testjava.entity;

import java.util.ArrayList;

public class Order_item_list {
    private ArrayList<Integer> products_ids;
    private ArrayList<Integer> products_quantities;

    public ArrayList<Integer> getProducts_ids() {
        return products_ids;
    }

    public ArrayList<Integer> getProducts_quantities(){
        return products_quantities;
    }

    public void setProducts_ids(ArrayList<Integer> products_ids) {
        this.products_ids = products_ids;
    }

    public void setProducts_quantities(ArrayList<Integer> products_quantities) {
        this.products_quantities = products_quantities;
    }
}
