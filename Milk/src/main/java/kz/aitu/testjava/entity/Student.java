package kz.aitu.testjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "students")
@Data
@NoArgsConstructor
@AllArgsConstructor//aksjdgajksdgjkas
public class Student {

    @Id
    private Long id;
    private String name;
    private String description;
    private String phone;

    @Column(name = "group_id")
    private Long groupId;
}
