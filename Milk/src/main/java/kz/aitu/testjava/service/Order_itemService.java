package kz.aitu.testjava.service;

import kz.aitu.testjava.entity.Auth;
import kz.aitu.testjava.entity.Order_item;
import kz.aitu.testjava.entity.Order_item_list;
import kz.aitu.testjava.repository.AuthRepository;
import kz.aitu.testjava.repository.CustomerRepository;
import kz.aitu.testjava.repository.Customer_orderRepository;
import kz.aitu.testjava.repository.Order_itemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@AllArgsConstructor
public class Order_itemService {

    private final Order_itemRepository order_itemRepository;
    private final AuthRepository authRepository;
    private final Customer_orderRepository customer_orderRepository;


    public List<Order_item> getAll() {
        return order_itemRepository.findAll();
    }

    public Order_item save(Order_item order_item) {
        return order_itemRepository.save(order_item);
    }

    public Order_item update(Order_item order_item) {

        Order_item order_itemDB = order_itemRepository.findById(order_item.getId()).orElse(null);
        if (order_itemDB == null) return null;

        order_itemDB.setQuantity(order_item.getQuantity());
        order_itemDB.setProduct_id(order_item.getProduct_id());
        return order_itemRepository.save(order_itemDB);
    }
    @Transactional
    public void insert(Order_item_list order_item_list){
        ArrayList<Integer> ids = order_item_list.getProducts_ids();
        ArrayList<Integer> quantities = order_item_list.getProducts_quantities();

        int order_id = customer_orderRepository.lastCustomer_order();
        for(int i=0; i<ids.size(); i++){
            int quantity = quantities.get(ids.get(i));
            order_itemRepository.insertOrder_item(ids.get(i),quantity,order_id+1);
        }

    }
    public Order_item getById(Long id) {
        return order_itemRepository.findById(id).orElse(null);
    }
}
