package kz.aitu.testjava.service;

import kz.aitu.testjava.entity.Auth;
import kz.aitu.testjava.entity.Customer;
import kz.aitu.testjava.entity.Customer_order;
import kz.aitu.testjava.repository.AuthRepository;
import kz.aitu.testjava.repository.CustomerRepository;
import kz.aitu.testjava.repository.Customer_orderRepository;
import kz.aitu.testjava.repository.Order_itemRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Service
@AllArgsConstructor
public class Customer_orderService {

    private final Customer_orderRepository customer_orderRepository;
    private final AuthRepository authRepository;
    private final CustomerRepository customerRepository;
    private final Order_itemRepository order_itemRepository;

    public List<Customer_order> getAll() {
        return customer_orderRepository.findAll();
    }

    public Customer_order save(Customer_order customer_order) {
        return customer_orderRepository.save(customer_order);
    }

    public void insert(long total_price, String token) throws Exception {
        LocalDate date = LocalDate.now();
        Auth auth = authRepository.findByToken(token);
        Customer customer = customerRepository.findById(auth.getCustomerId()).orElseThrow(ChangeSetPersister.NotFoundException::new);
        customer_orderRepository.insertCustomer_order(total_price,customer.getId(),"waiting_for_dispatch", date);
    }

    public Customer_order update(Customer_order customer_order) {

        Customer_order customer_orderDB = customer_orderRepository.findById(customer_order.getId()).orElse(null);
        if (customer_orderDB == null) return null;

        customer_orderDB.setTotal_price(customer_order.getTotal_price());
        return customer_orderRepository.save(customer_orderDB);
    }

    public Customer_order getById(Long id) {
        return customer_orderRepository.findById(id).orElse(null);
    }

    public int lastCustomer_order(){
        return customer_orderRepository.lastCustomer_order();
    }

    public List<Customer_order> getByCustomer_id(Long customer_id) {
        return customer_orderRepository.findAllByCustomerId(customer_id);
    }

    public List<Customer_order> getByStatusAndCustomerId(long customer_id,String status) {
        return customer_orderRepository.findAllByStatusAndCustomerId(status,customer_id);
    }

    @Transactional
    public void deleteById(long id) {
        customer_orderRepository.deleteById(id);
        order_itemRepository.deleteAllByOrderId(id);
    }
}
