package kz.aitu.testjava.service;

import kz.aitu.testjava.entity.Group;
import kz.aitu.testjava.repository.GroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class GroupService {

    private final GroupRepository groupRepository;

    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    public Group save(Group group) {
        return groupRepository.save(group);
    }

    public Group update(Group group) {

        Group groupDB = groupRepository.findById(group.getId()).orElse(null);
        if (groupDB == null) return null;

        groupDB.setName(group.getName());
        return groupRepository.save(groupDB);
    }

    public Group getById(Long id) {
        return groupRepository.findById(id).orElse(null);
    }
}
