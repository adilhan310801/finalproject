INSERT INTO Product  VALUES
(1,'Milk','drinking milk and cream',250,1),
(2,'Cream','drinking milk and cream',1100,1),
(3,'Kefir','drinking milk and cream',300,1),
(4,'Buffer','cow butter',600,3),
(5,'Yogurt','dairy products',350,2),
(6,'Sour cream','dairy products',600,2),
(7,'Cottage cheese','dairy products',450,2),
(8,'Kumys','dairy products',1500,2),
(9,'Katyk','dairy products',200,2),
(10,'Cheese','cheeses',1250,4),
(11,'Fermented baked milk','dairy products',250,2),
(12,'Ice cream','ice cream',250,6),
(13,'Condensed milk','canned milk',250,5),
(14,' Dry Cream','canned milk',1200,5),
(15,' Glazed curds','dairy products',100,2);


INSERT INTO Category VALUES
(1,'drinking milk and cream','Dairy product obtained from whole milk by separating the fat fraction'),
(2,'dairy products','All natural products from milk that are obtained by lactic acid or mixed fermentation'),
(3,'cow butter','Creamy and melted butter'),
(4,'cheeses','rennet and fermented milk'),
(5,'canned milk','condensed and dry milk products'),
(6,'ice cream','Dairy product with added vegetable fats');


INSERT INTO Customer VALUES
(1,'smn','smth','8777777777','a@a.com'),
(2,'smn1','smth1','87777777771','a@a.com1');

INSERT INTO Auth VALUES
(1,1,'first','pass'),
(2,2,'second','pass');


CREATE TABLE Customer_order (
id serial PRIMARY KEY,
total_price bigint,
customer_id bigint
);

CREATE TABLE Order_item (
id serial PRIMARY KEY,
product_id bigint ,
quantity bigint,
order_id bigint
);
