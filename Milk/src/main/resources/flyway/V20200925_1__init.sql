create table students (
    id serial,
    name varchar(255),
    description varchar(255),
    phone varchar(255),
    group_id bigint
);

create table groups (
                         id serial,
                         name varchar(255),
                         description varchar(255)
);
